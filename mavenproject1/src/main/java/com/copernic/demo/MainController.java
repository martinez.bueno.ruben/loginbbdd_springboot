/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.demo;

import com.copernic.demo.dao.PersonDAO;
import com.copernic.demo.domain.Person;
import com.copernic.demo.services.PersonService;
import com.copernic.demo.services.loginService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Ruben
 */
@Controller
@Slf4j
public class MainController {

    @Autowired
    private loginService loginService;
    @Autowired
    private PersonService PersonService;

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @PostMapping("/login")
    public String signUp(
    @RequestParam(name = "user",required = true)String user,
    @RequestParam(name = "pass",required = true)String pass,
    Model model,Person persona) {
        String comprovacion = loginService.comprovarUsuario(persona,user,pass);
        return comprovacion;

    }
    
    @GetMapping("/lista")
    public String lista(Model model){
        List<Person> persons = PersonService.getAllPersons();
        model.addAttribute("persons",persons);
        return "lista";
        
    }
    
    @GetMapping("/pagina")
    public String pagina() {
        return "pagina";
    }

    @GetMapping("/registro")
    public String registro() {
        return "registro";
    }

    @PostMapping("/registro")
    public String registroDatos(Person persona,Model model) {
        PersonService.savePerson(persona);
        model.addAttribute("person",persona);
        return "pagina";

    }
    
    @GetMapping("/accedido")
    public String accedido(){
        return "accedido";
    }
    
    @GetMapping("/update/{id}")
    public String update(Person person,Model model){
        person = PersonService.findPerson(person);
        model.addAttribute("person",person);
        return "update";
    }
    @GetMapping("/delete/{id}")
    public String confirmacion(@PathVariable long id,Model model){
        Person person = PersonService.getPersonById(id);
        model.addAttribute("person",person);
        return "delete";
        
    }
    @GetMapping("/deleteOK/{id}")
    public String deletePerson(Person person){
        PersonService.deletePerson(person.getId());
        return  "redirect:/lista";
    }

}
